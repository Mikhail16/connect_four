import React from 'react';
import './ItemField.css';

function ItemField({ column, playerId, handleItemClick }) {
    const itemClasses = playerId === null ? 'ItemField' : playerId === 1 ? 'ItemField yellow' : 'ItemField red';

    return (
        <div className={itemClasses} onClick={() => handleItemClick(column)}></div>
    );
}

export default ItemField;