import React from 'react';
import './MessageBlock.css';

function MessageBlock({ state }) {
    return (
        state.gameFinished ?
            <div className="MessageBlock">
                <div className="msg-bl">
                    <h3>Победил игрок №<b>{state.playerTurn}</b></h3>
                </div>
            </div>
            :
            null
    );
};

export default MessageBlock;