import React from 'react';
import ItemField from '../ItemField';
import './GameField.css';

function GameField({ state, handleItemClick }) {
    let columnsCountGroups = state.stateFieldItems.map(item =>
        <ItemField key={item.position}
            column={item.position[1]}
            playerId={item.playerId}
            handleItemClick={handleItemClick} />
    );

    let fieldStyle = {
        pointerEvents: state.gameFinished ? 'none' : 'auto',
        opacity: state.gameFinished ? 0.4 : 1
    };

    return (
        <div className="GameField" style={fieldStyle}>{columnsCountGroups}</div>
    );
}

export default GameField;