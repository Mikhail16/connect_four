import React from 'react';
import './InfoBlock.css';

function InfoBlock({ state, handNewGame }) {
    return (
        <div className="InfoBlock">
            <div className="col">
                Ход игрока №<b>{state.playerTurn}</b>
            </div>
            <div className="col">
                Общий счет:
                <div className="score">
                    <span>{state.gameScore.firstPlayer}</span>
                    :
                    <span>{state.gameScore.secondPlayer}</span>
                </div>
            </div>
            <div className="col">
                <button onClick={() => handNewGame()}>Новая игра</button>
            </div>
        </div>
    );
}

export default InfoBlock;