import React from 'react';
import GameField from './components/GameField';
import InfoBlock from './components/InfoBlock';
import MessageBlock from './components/MessageBlock';
import './App.css';

function App() {
    const columnsCount = 7;
    const rowsCount = 6;

    const getClealFields = () => {
        let stateFieldItems = [];
        for (let i = 0; i < rowsCount; i++) {
            for (let j = 0; j < columnsCount; j++) {
                stateFieldItems = [...stateFieldItems, {
                    position: [i, j],
                    playerId: null
                }];
            }
        }

        return stateFieldItems;
    }

    const [state, setState] = React.useState({
        stateFieldItems: getClealFields(),
        playerTurn: 1,
        gameFinished: false,
        gameScore: {
            firstPlayer: 0,
            secondPlayer: 0
        }
    });

    // Find all items in this column
    const findItemsInColumn = (arrNewState, column) => {
        let arrItemsColumn = arrNewState.filter(item => item.position[1] === column);
        // Reverse
        arrItemsColumn = arrItemsColumn.reverse();
        return arrItemsColumn;
    }

    // Find position
    const findPosition = (arrItemsColumn) => {
        let position = null;

        for (let i = 0; i < arrItemsColumn.length; i++) {
            const item = arrItemsColumn[i];

            if (item.playerId == null) {
                position = item.position;
                break;
            }
        }

        return position;
    }

    // Change item in state
    const changeItem = (newStateFieldItems, position) => {
        for (let i = 0; i < newStateFieldItems.length; i++) {
            const item = newStateFieldItems[i];

            if (item.position === position) {
                item.playerId = state.playerTurn;
                break;
            }
        }

        return newStateFieldItems;
    }

    // Array items from row
    const getArrayFromRow = (arr, rowNumber) => {
        let arrFromRow = arr.filter(item => item.position[0] === rowNumber);
        return arrFromRow;
    }

    // Array items from column
    const getArrayFromColumn = (arr, columnNumber) => {
        let arrFromRow = arr.filter(item => item.position[1] === columnNumber);
        return arrFromRow;
    }

    // Array items from diagonals
    const getArrayFromFirstDiagonal = (arr, position) => {
        let arrFromDiagonal = arr.filter(item => {
            return item.position[0] + item.position[1] === position[0] + position[1];
        });

        return arrFromDiagonal;
    }

    // Array items from diagonals
    const getArrayFromSecondDiagonal = (arr, position) => {
        let arrFromDiagonal = arr.filter(item => {
            return item.position[0] - item.position[1] === position[0] - position[1];
        });

        return arrFromDiagonal;
    }

    //
    const checkLine = (arr) => {
        let playerId = null;

        for (let i = 0; i < arr.length; i++) {
            const arrLine = arr[i];

            for (let j = 3; j < arrLine.length; j++) {
                const item = arrLine[j];

                if ([arrLine[j - 3].playerId, arrLine[j - 2].playerId, arrLine[j - 1].playerId].every(el => el === item.playerId)) {
                    playerId = item.playerId;
                    break;
                }
            }

            if (playerId !== null) {
                break;
            }
        }

        return playerId;
    }

    // Проверим не заполнено ли поле
    const checkFieldFill = (arr) => {
        let fieldFill = arr.every(item => item.playerId !== null);
        return fieldFill;
    }

    const setWinner = (playerId, arr) => {
        // Проверим не заполнено ли поле
        const fieldFill = checkFieldFill(arr);
        let gameScore = state.gameScore;

        if (fieldFill) {
            // Обновим счет
            gameScore.firstPlayer += 1;
            gameScore.secondPlayer += 1;
        }
        else {
            if (playerId === 1) {
                // Обновим счет для 1 игрока
                gameScore.firstPlayer += 1;
            }
            else {
                // Обновим счет для 2 игрока
                gameScore.secondPlayer += 1;
            }
        }

        // Установим новые значения для state
        setState({
            stateFieldItems: arr,
            playerTurn: state.playerTurn,
            gameFinished: true,
            gameScore
        });
    }

    // Check winner
    const checkWinner = (arr, position) => {
        // Массив по всем направлениям, которые нужно проверить
        const checkArrays = [
            getArrayFromRow(arr, position[0]),
            getArrayFromColumn(arr, position[1]),
            getArrayFromFirstDiagonal(arr, position),
            getArrayFromSecondDiagonal(arr, position)
        ];
        // Если есть победитель получим его id
        let winnerPlayerId = checkLine(checkArrays);

        if (winnerPlayerId !== null) {
            setWinner(winnerPlayerId, arr);
        }
        else {
            // Установим новые значения для state
            setState({
                stateFieldItems: arr,
                playerTurn: state.playerTurn === 1 ? 2 : 1,
                gameFinished: state.gameFinished,
                gameScore: state.gameScore
            });
        }
    }

    const handNewGame = () => {
        setState({
            stateFieldItems: getClealFields(),
            playerTurn: 1,
            gameFinished: false,
            gameScore: state.gameScore
        });
    }

    // Click on item in field
    const handleItemClick = (column) => {
        // Create new arr from state
        let newStateFieldItems = state.stateFieldItems.slice();
        // Find all items in this column
        const arrItemsColumn = findItemsInColumn(newStateFieldItems, column);
        // Position of item in column which will be change
        let position = findPosition(arrItemsColumn);
        // 
        if (position === null) {
            // проверить не заполнена ли вся игра
            console.log('Choose another column');
            return;
        }
        // Обновим данные для state
        newStateFieldItems = changeItem(newStateFieldItems, position)
        // Проверим, возможно, есть победитель
        checkWinner(newStateFieldItems, position);
    }

    return (
        <div className="App">
            <h1>Connect four</h1>
            <InfoBlock state={state} handNewGame={handNewGame} />
            <GameField state={state} handleItemClick={handleItemClick} />
            <MessageBlock state={state} />
        </div>
    );
}

export default App;